$(document).ready(function() {

    $('.review-slider').slick({
        // dots: true,
        autoplay: true,
        autoplaySpeed: 6000,
        infinite: true,
        speed: 800,
        slidesToShow: 6,
        slidesToScroll: 1,
        responsive: [{
            breakpoint: 1024,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 1,
                // centerMode: true,

            }

        }, {
            breakpoint: 800,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 1,
                dots: false,
                infinite: true,

            }


        }, {
            breakpoint: 600,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
                dots: false,
                infinite: true,
                
            }
        }, {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: false,
                infinite: true,
                autoplay: true,
                autoplaySpeed: 4000,
            }
        }]
    });
  
    $('.slide-bar').slick({
        // dots: true,
        autoplay: true,
        autoplaySpeed: 1000,
        infinite: true,
        speed: 400,
        slidesToShow: 4,
      	arrows: false,
        responsive: [{
            breakpoint: 800,
            settings: {
                slidesToShow: 3,
                dots: false,
                infinite: true,
            }
        }, {
            breakpoint: 600,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 2,
                dots: false,
                infinite: true,    
            }
        }, {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: false,
                infinite: true,
                autoplay: true,
                autoplaySpeed: 4000,
            }
        }]
    });
  
  	$('.slide-bars').slick({
        // dots: true,
        autoplay: true,
        autoplaySpeed: 1000,
        infinite: true,
        speed: 400,
        slidesToShow: 3,
      	arrows: false,
        responsive: [{
            breakpoint: 800,
            settings: {
                slidesToShow: 3,
                dots: false,
                infinite: true,
            }
        }, {
            breakpoint: 600,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 2,
                dots: false,
                infinite: true,    
            }
        }, {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: false,
                infinite: true,
                autoplay: true,
                autoplaySpeed: 4000,
            }
        }]
    });
  
  	$('.collection-slider').slick({
        // dots: true,
        autoplay: true,
        autoplaySpeed: 6000,
        infinite: true,
        speed: 800,
        slidesToShow: 4,
        slidesToScroll: 1,
        responsive: [{
            breakpoint: 1024,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 1,
                // centerMode: true,

            }

        }, {
            breakpoint: 800,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 1,
                dots: false,
                infinite: true,

            }


        }, {
            breakpoint: 600,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
                dots: false,
                infinite: true,
                
            }
        }, {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: false,
                infinite: true,
                autoplay: true,
                autoplaySpeed: 4000,
            }
        }]
    });

});
